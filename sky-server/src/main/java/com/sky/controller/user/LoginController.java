package com.sky.controller.user;

import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.properties.JwtProperties;
import com.sky.result.Result;
import com.sky.service.UserService;
import com.sky.utils.JwtUtil;
import com.sky.vo.UserLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user/user")
@Api(tags = "用户端登录相关接口")
@Slf4j
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProperties jwtProperties;

    @PostMapping("/login")
    @ApiOperation("用户登录接口")
    public Result<UserLoginVO> login(@RequestBody UserLoginDTO userLoginDTO){
        //1.接收参数
        log.info("微信登录的授权码：{}",userLoginDTO.getCode());

        //2.调用service层，实现业务逻辑处理
        User user = userService.login(userLoginDTO);

        //3.生成令牌  openid
        //令牌中的自定义信息   header.payload.signature
        Map<String, Object> claims = new HashMap<>();
        claims.put("openid",user.getOpenid());
        claims.put("userId",user.getId());

        String jwt = JwtUtil.createJWT(jwtProperties.getUserSecretKey(), jwtProperties.getUserTtl(), claims);
        //4.组装vo对象，返回给小程序
        UserLoginVO userLoginVO =
                UserLoginVO.builder()
                        .id(user.getId())
                        .openid(user.getOpenid())
                        .token(jwt)
                        .build();
        //5.响应数据
        return Result.success(userLoginVO);
    }
}
