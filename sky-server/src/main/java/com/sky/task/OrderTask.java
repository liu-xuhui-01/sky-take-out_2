package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrdersMapper;
import com.sky.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@Component
@Slf4j
public class OrderTask {

    @Autowired
    OrdersMapper ordersMapper;

    /**
     * 处理支付超时订单
     */
   // @Scheduled(cron = "5 * * * * * ?")
    public void noPay() {

        log.info("noPay{}", LocalDateTime.now());
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-15);

        List<Orders> ordersList = ordersMapper.getByStatusAndCreateTimeLt(Orders.PENDING_PAYMENT, localDateTime);
        if (ordersList!=null && ordersList.size()>0){
            for (Orders orders : ordersList) {
                orders.setStatus(Orders.CANCELLED);
                orders.setCancelReason("订单超时未支付");
                orders.setOrderTime(LocalDateTime.now());

                ordersMapper.update(orders);
            }

        }

    }

    /**
     * 处理“派送中”状态的订单
     */
   // @Scheduled(cron = "0 0 1 * * * ?")
    public void noComlate() {

        log.info("noPay{}", LocalDateTime.now());
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(-60);

        List<Orders> ordersList = ordersMapper.getByStatusAndCreateTimeLt(Orders.DELIVERY_IN_PROGRESS, localDateTime);
        if (ordersList!=null && ordersList.size()>0){
            for (Orders orders : ordersList) {
                orders.setStatus(Orders.COMPLETED);
                ordersMapper.update(orders);
            }

        }

    }


}
